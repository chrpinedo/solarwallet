# Maintainer: Mark Wagie <mark dot wagie at tutanota dot com>
# Co-Maintainer: Christian Pinedo <christian (at) chrpinedo (dot) me>
pkgname=solarwallet
pkgver=0.25.1
pkgrel=1
pkgdesc="Wallet for the Stellar payment network by SatoshiPay."
arch=('x86_64')
url="https://solarwallet.io"
license=('MIT')
depends=('libnotify' 'nss' 'libxss' 'libxtst' 'xdg-utils' 'libappindicator-gtk3' 'libsecret')
makedepends=('npm')
conflicts=('satoshipay-stellar-wallet')
replaces=('satoshipay-stellar-wallet')
source=("$pkgname-$pkgver.tar.gz::https://github.com/satoshipay/solar/archive/v$pkgver.tar.gz"
        "$pkgname.desktop")
sha256sums=('a7bcb3ef5f3a6820f681a45a070162ee1075b1def92f6f58cea8553b2a240a61'
            'fcbc7f3591ae0565024d635b6182f84a9d24453c86176bfafedd8c56c54faef2')

build() {
	cd "solar-$pkgver"
	npm install --cache "$srcdir/npm-cache"
	npm run build:linux
}

package() {
	cd "solar-$pkgver"

	# Install directories
	install -dm755 "$pkgdir/"{opt/"$pkgname",usr/bin}

	# Remove files not needed
	rm -rf electron/dist/linux-unpacked/resources/app.asar.unpacked

	# Copy main files
	cp -a electron/dist/linux-unpacked/* "$pkgdir/opt/$pkgname"

	# Link to the binary
	ln -sf "/opt/$pkgname/io.solarwallet.app" "$pkgdir/usr/bin/$pkgname"

	# Install icon
	install -Dm644 electron/build/icons/512.png "$pkgdir/usr/share/pixmaps/$pkgname.png"

	# Install desktop file
	install -Dm644 "$srcdir/$pkgname.desktop" -t "$pkgdir/usr/share/applications"

	# Install license
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
